//
//  Constants.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 10/20/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import Foundation

struct Constants {
    
    struct Storyboard {
        
        static let HomeViewController = "HomeVC"
        static let ColorBlindViewController = "ColorBlindVC"
        static let ColorBlindTypeViewController = "ColorBlindTypeVC"
        static let SelectTrendViewController = "SelectTrendVC"
        static let LogInViewController = "LogInVC"
        static let ClotheDetailViewController = "ClotheDetailVC"
        static let CategoriesViewController = "CategoriesVC"
        static let ResetPasswordViewController = "ResetPasswordVC"
    }
    
    
}
