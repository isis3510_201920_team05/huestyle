//
//  Utilities.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 10/20/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import Foundation
import UIKit

class Utilities {
    
    static func isPasswordValid(_ password : String) -> Bool {
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: password)
    }
    
}

