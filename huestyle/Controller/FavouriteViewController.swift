//
//  FavouriteViewController.swift
//  huestyle
//
//  Created by Oliver Amaya on 10/11/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase


class FavouriteViewController: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    
    var favoriteArray = [String]()
    var clothesFavorite = [Clothes]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(UINib(nibName: "FavouriteTableViewCell", bundle: nil), forCellReuseIdentifier: "FavouriteTableViewCell")
        setUpUserData()
        // Do any additional setup after loading the view.
    }
    
    
    func setUpUserData() {
        let clothingReference = Firestore.firestore().collection("clothing")
        
        clothingReference.addSnapshotListener { (snapshot, _) in
        
        let uid = String(Auth.auth().currentUser!.uid);
        let db = Firestore.firestore()
        db.collection("users")
            .whereField("uid", isEqualTo: uid)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting user data", err)
                } else {
                    guard let document = querySnapshot?.documents.first else {
                        print("Error")
                        return
                    }
                    
                    self.favoriteArray =  document.get("favorites") as! [String]
                    guard let snapshot = snapshot else {return}
                    for favorite in self.favoriteArray{
                        for document in snapshot.documents{
                            let myData = document.data()
                            let url = myData["src"] as? String
                            let name = myData["name"] as? String
                            let price = myData["price"] as! Int
                            let category = myData["category"] as? String
                            let tags = myData["tags"] as? String
                            let clothesFinal = Clothes(url: url!, id: "", name: name!, price: price, sizes: "", tags: tags ?? "", category: category!, stores: [])
                            
                            if (name == favorite){
                                self.clothesFavorite.append(clothesFinal)
                            }
                            
                        }
                        
                        
                    }
                    
                    print(self.favoriteArray)
                    self.tblView.reloadData()
                
                }
                self.tblView.reloadData()
        }
        }
    }
}


extension FavouriteViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoriteArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let image = clothesFavorite[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteTableViewCell") as! FavouriteTableViewCell
        cell.lbl_Tag.layer.cornerRadius = 15.0
        cell.lbl_Tag.clipsToBounds = true
        cell.lbl_Title.text = favoriteArray[indexPath.row]
        cell.lbl_Price.text = "$\(image.price!)"
        cell.imgView_image.sd_setImage(with: URL(string: image.url!), placeholderImage: UIImage(named: "image1"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
