//
//  HeaderCollectionViewCell.swift
//  huestyle
//
//  Created by Oliver Amaya on 10/11/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit

class HeaderCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var view_Outer: UIView!
    @IBOutlet weak var lbl_Title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
