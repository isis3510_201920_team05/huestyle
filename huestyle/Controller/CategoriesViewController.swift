//
//  CategoriesViewController.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 10/20/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore
import SDWebImage

class CategoriesViewController: UICollectionViewController, UISearchBarDelegate {
    
    @IBOutlet var ClothesCollectionView: UICollectionView!
    
    
    //Firestore setup constants
    var db: Firestore!
    let uid = String(Auth.auth().currentUser!.uid);
    
    //Images setup
    var clothes = [Clothes]()
    var customImageFlowLayout: CustomImageFlowLayout!
    var headerView: CategoriesHeaderView?
    var favoriteClothes: [String] = []

    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.searchBar.scopeButtonTitles = ["All", "Jeans", "T-Shirts"]
        searchController.searchBar.delegate = self
        
        loadFavList()
        loadImagesDB ()
        // Do any additional setup after loading the view
        customImageFlowLayout = CustomImageFlowLayout()
        ClothesCollectionView.collectionViewLayout = customImageFlowLayout
        ClothesCollectionView.register(CategoriesHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "CartHeaderCollectionReusableView2")
        
    }
    
    func applySearch(searchText: String, scope: String = "All"){
        if searchController.searchBar.text! == "" {
            
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        headerView = ClothesCollectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CartHeaderCollectionReusableView2", for: indexPath) as? CategoriesHeaderView
        return headerView!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return .init(width: view.frame.width, height: 50)
    }
    
    func loadImagesDB (){
        
        let clothingReference = Firestore.firestore().collection("clothing")
        var newDataClothes = [Clothes]()
        
        clothingReference.addSnapshotListener { (snapshot, _) in
            Firestore.firestore().collection("users")
                .whereField("uid", isEqualTo: self.uid)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print("Error getting user data", err)
                } else {
                    guard (querySnapshot?.documents.first) != nil else {
                    print("Error")
                   return
                 }
                    guard let snapshot = snapshot else {return}
                    for document in snapshot.documents{
                        let myData = document.data()
                        let url = myData["src"] as? String
                        let name = myData["name"] as? String
                        let price = myData["price"] as! Int
                        let sizes = myData["sizes"] as! String
                        let category = myData["category"] as? String
                        
                        let clothesData = Clothes(url: url!, id: "", name: name!, price: price, sizes: sizes, tags: "", category: category!, stores: [])
                        
                        newDataClothes.append(clothesData)
                    }
                    self.clothes = newDataClothes
                    self.ClothesCollectionView.reloadData()
                }
            }
        }
    }
    
    func loadFavList(){
        let uid = String(Auth.auth().currentUser!.uid);
        let dbf = Firestore.firestore()
        
        dbf.collection("users")
        .whereField("uid", isEqualTo: uid)
        .getDocuments() { (querySnapshot, err) in
            if let err = err {
                // Some error occured
                print("Error getting user data", err)
            } else {
             guard let document = querySnapshot?.documents.first else {
               print("Error")
               return
             }
                self.favoriteClothes = document.get("favorites") as! [String]

            }
        }
    }

    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return clothes.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = ClothesCollectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ClotheCollectionViewCell
        
        let clothe = clothes[indexPath.row]
        
        cell.ClotheImage.sd_setImage(with: URL(string: clothe.url!), placeholderImage: UIImage(named: "image1"))
        
        if favoriteClothes.contains(clothe.name!) {
            cell.FavoriteButton.backgroundColor = UIColor.black
            cell.isFav = true
        } else {
            cell.isFav = false
        }
        
        cell.TitleLabel.text = clothe.name
        cell.PriceLabel.text = "Price: \(clothe.price ?? 0)"
        
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.5
        
        let tap =  UITapGestureRecognizer(target: self, action: #selector(self.TappedCell(sender:)))
        
        tap.name = "\(indexPath.row)"

        cell.addGestureRecognizer(tap)
        
        
        
        return cell
    }
    
    @objc func TappedCell(sender: UITapGestureRecognizer ) {
           
        let clothe = clothes[Int(sender.name!)!]
        
        let clotheDetailViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.ClotheDetailViewController) as? ClotheDetailViewController
           
           clotheDetailViewController!.clothes = clothe
           clotheDetailViewController?.controller = "CategoriesVC"
           self.view.window?.rootViewController = clotheDetailViewController
           self.view.window?.makeKeyAndVisible()
    }
    
}

