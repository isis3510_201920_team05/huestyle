//
//  ImageCollectionViewCell.swift
//  huestyle
//
//  Created by Oliver Amaya on 12/10/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var itemName: UILabel!
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.image = nil
    }
}
