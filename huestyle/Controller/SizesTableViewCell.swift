//
//  SizesTableViewCell.swift
//  huestyle
//
//  Created by Oliver Amaya on 10/11/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit

class SizesTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl_Size: UILabel!
    @IBOutlet weak var lbl_Status: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
