//
//  LogInViewController.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 10/6/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit
import SwiftUI
import Firebase
import SVProgressHUD

class LogInViewController: UIViewController, ReachabilityObserverDelegate {
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var logInButton: UIButton!
    
    @IBOutlet weak var errorMessageLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.text = "test@gmail.com"
        passwordTextField.text = "Apple@123"
        
        addReachabilityObserver()
        // Do any additional setup after loading the view.
        
        setUpElements()
        
        self.setupToHideKeyboardOnTapOnView()

    }
    
    func reachabilityChanged(_ isReachable: Bool) {
        if !isReachable {
            self.showError("Verify your internet conection")
        }
        if isReachable{
            self.dontShowError()
        }
    }

    func setUpElements() {
        // Hide the error label
        errorMessageLabel.alpha = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func forgotPasswordTapped(_ sender: Any) {
        let resetPasswordViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.ResetPasswordViewController) as? ResetPasswordViewController
        
        self.view.window?.rootViewController = resetPasswordViewController
        self.view.window?.makeKeyAndVisible()
    }
    
    @IBAction func logInTapped(_ sender: Any) {
        
        
        //SVProgressHUD.show()
        let email = emailTextField.text!
        let password = passwordTextField.text!

        if(email.count > 30){
            errorMessageLabel.text = "Email can not have more than 30 characters"
            errorMessageLabel.alpha = 1
            
        }
        else {
            if(password.count > 15){
                errorMessageLabel.text = "Password can not have more than 15 characters"
                errorMessageLabel.alpha = 1
            } else {
                // TODO: Validate Text Fields
                // Signing in the user
                Auth.auth().signIn(withEmail: emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines), password: passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)) { (user, error) in
                    if error != nil {
                        // Couldn't sign in
                        print("no pudo iniciar")
                        self.errorMessageLabel.text = error!.localizedDescription
                        self.errorMessageLabel.alpha = 1
                        //SVProgressHUD.dismiss()
                    }
                    else {
                        print("Log in successful!")
                        
                        //SVProgressHUD.dismiss()

                        //self.performSegue(withIdentifier: "goToHome", sender: nil)
                        let homeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.HomeViewController) as? HomeViewController
                        
                        self.view.window?.rootViewController = homeViewController
                        self.view.window?.makeKeyAndVisible()
                    }
                }
            }
        }
        
    }
    
    func showError(_ message:String) {
        
        errorMessageLabel.text = message
        errorMessageLabel.alpha = 1
    }
    func dontShowError() {
        
        errorMessageLabel.text = ""
        errorMessageLabel.alpha = 0
    }
    
}
extension UIViewController
{
    func setupToHideKeyboardOnTapOnView()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))

        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}

