//
//  ListViewController.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 10/6/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit
import SwiftUI
import Firebase
import FirebaseAuth
import FirebaseFirestore
import SDWebImage
import SVProgressHUD
    
class ListViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
        
    @IBOutlet weak var titleHeader: UILabel!
    @IBOutlet weak var imageCollection: UICollectionView!
    var window: UIWindow?
    
    var images = [Clothes]()
    var customImageFlowLayout: CustomImageFlowLayout!
    var db: Firestore!
    let id = String(Auth.auth().currentUser!.uid);
    var headerView: ListHeaderView?
    var totalProductCount = 20
    var userClothesPreference = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        getSTtyleTRype()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    fileprivate func setupLayout() {
        customImageFlowLayout = CustomImageFlowLayout()
        imageCollection.collectionViewLayout = customImageFlowLayout
        imageCollection.register(ListHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "CartHeaderCollectionReusableView")
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        headerView = imageCollection.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CartHeaderCollectionReusableView", for: indexPath) as? ListHeaderView
        return headerView!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return .init(width: view.frame.width, height: 50)
    }
    
    @IBSegueAction func addSwiftUIView(_ coder: NSCoder) -> UIViewController? {
        return UIHostingController(coder: coder, rootView: ListView())
    }
    
    func getSTtyleTRype() {
        Firestore.firestore().collection("users")
            .whereField("uid", isEqualTo: self.id)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print("Error getting user data", err)
                    
                } else {
                    guard let document = querySnapshot?.documents.first else {
                        print("Error")
                        return
                    }
                    self.userClothesPreference = (document.get("styleType") as? String)!
                    self.loadImagesDB(self.totalProductCount)
                }
        }
        
    }
    
    func loadImagesDB (_ id : Int){
        

        let clothingReference = Firestore.firestore().collection("clothing").whereField("trend", isEqualTo: self.userClothesPreference)
        
        var newImages = [Clothes]()
        
        clothingReference.limit(to: id).order(by: "id").addSnapshotListener { (snapshot, _) in

        


                    

                    guard let snapshot = snapshot else {return}
                    for document in snapshot.documents{
                        let myData = document.data()
                        print(myData["id"])
                        let url = myData["src"] as? String
                        let name = myData["name"] as? String
                        let clotheTrend = myData["trend"] as! String
                        let price = myData["price"] as! Int
                        let category = myData["category"] as? String
                        let tags = myData["tags"] as? String
                        let storess =  myData["stores"] as! [Int]
                        let clothesFinal = Clothes(url: url!, id: "", name: name!, price: price, sizes: "", tags: tags ?? "", category: category!, stores: storess)


                                newImages.append(clothesFinal)

                    self.images = newImages
                    self.imageCollection.reloadData()

                }
            }
        }

    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    private func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let size = CGSize(width: 50, height: 50)
        return size
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell = imageCollection.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ImageCollectionViewCell
        
        let image = images[indexPath.row]
        
        cell.imageView.sd_setImage(with: URL(string: image.url!), placeholderImage: UIImage(named: "image1"))
        cell.itemName.text = image.name
        cell.itemPrice.text = "$\(image.price!)"
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 0.5
      
        let tap =  UITapGestureRecognizer(target: self, action: #selector(self.TappedCell(sender:)))
        
        tap.name = "\(indexPath.row)"
        cell.addGestureRecognizer(tap)

        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == self.images.count - 4 {
            self.totalProductCount += 20
            self.loadImagesDB(self.totalProductCount)
        }
    }
    
    @objc func TappedCell(sender: UITapGestureRecognizer ) {
        
        let clothe = images[Int(sender.name!)!]
        
        let clotheDetailViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.ClotheDetailViewController) as! ClotheDetailViewController
        
        clotheDetailViewController.clothes = clothe
        clotheDetailViewController.controller = "HomeVC"
        self.navigationController?.pushViewController(clotheDetailViewController, animated: true)
    }
    
    
}

