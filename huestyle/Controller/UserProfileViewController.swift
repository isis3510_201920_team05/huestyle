//
//  UserProfileViewController.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 10/11/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class UserProfileViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var trendStyleLabel: UILabel!
    
    @IBOutlet weak var logOutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpUserData()
        
        //self.nameLabel = Auth.auth().name
        // Do any additional setup after loading the view.
    }
    
    func setUpUserData() {
        let uid = String(Auth.auth().currentUser!.uid);
                
        let db = Firestore.firestore()
                
        
       db.collection("users")
       .whereField("uid", isEqualTo: uid)
       .getDocuments() { (querySnapshot, err) in
           if let err = err {
               // Some error occured
               print("Error getting user data", err)
           } else {
            guard let document = querySnapshot?.documents.first else {
              print("Error")
              return
            }
            //print(document.data())
            let name = document.get("name") as! String
            let email = Auth.auth().currentUser!.email
            let style = document.get("styleType") as! String

            self.nameLabel.text = name
            self.emailLabel.text = email
            self.trendStyleLabel.text = style

           }
       }
       
    }
    
    @IBAction func logOutButtonTapped(_ sender: Any) {
        
        do {
            try Auth.auth().signOut()
            
            let logInViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.LogInViewController) as? LogInViewController
            
            self.view.window?.rootViewController = logInViewController
            self.view.window?.makeKeyAndVisible()
            
        } catch let error {
            // handle error here
            print("Error trying to sign out of Firebase: \(error.localizedDescription)")
        }
        
    }
    
    

}
