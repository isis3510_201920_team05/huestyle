//
//  ClotheDetailViewController.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 10/16/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleMaps
import MapKit
import Firebase
import FirebaseFirestore

class ClotheDetailViewController: UIViewController {
    
    @IBOutlet weak var ClotheImage: UIImageView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var colnView_Height: NSLayoutConstraint!
    @IBOutlet weak var colnView: UICollectionView!
    @IBOutlet weak var tblView_Sizes: UITableView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var txtView_Description: UITextView!
    
    var controller = ""
    var clothes = Clothes(url: "", id: "", name: "", price: 0, sizes: "", tags: "", category: "", stores: [])
    let array = ["XS","S","M","L","XL","XXL"]
    var arraySizes = [String]()
    var arrayTags = [String]()
    var arrayStores = [[String:Any]]()
    var locationMarker: GMSMarker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colnView.register(UINib(nibName: "HeaderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HeaderCollectionViewCell")
        print("clothes: ",clothes)
        tblView_Sizes.separatorStyle = .none
        tblView_Sizes.register(UINib(nibName: "SizesTableViewCell", bundle: nil), forCellReuseIdentifier: "SizesTableViewCell")
        ClotheImage.sd_setImage(with: URL(string: clothes.url!), placeholderImage: UIImage(named: "image1"))
        nameLabel.text = clothes.name!
        priceLabel.text = "Price: \(clothes.price!)"
        
        arraySizes = clothes.sizes?.components(separatedBy: ",") ?? []
        arrayTags = clothes.tags?.components(separatedBy: ",") ?? []
        if arrayTags.count == 0 {
            self.colnView_Height.constant = 0.0
        } else {
            self.colnView_Height.constant = 50.0
        }
        self.colnView.reloadData()
        self.tblView_Sizes.reloadData()
        self.getStoresCordinates(clothes.stores)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func bntnAction_Like(_ sender: Any) {
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getStoresCordinates(_ data:[Int]) {
        let clothingReference = Firestore.firestore().collection("stores")
        for i in 0..<data.count {
            clothingReference.document("\(data[i])").addSnapshotListener { (snapShot, error) in
                let data = snapShot?.data()!
                self.arrayStores.append(data!)
                self.setMarker(data!)
            }
        }
        
    }
}

extension ClotheDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SizesTableViewCell") as! SizesTableViewCell
        cell.lbl_Size.text = array[indexPath.row]
        if arraySizes.contains(array[indexPath.row]) {
            cell.lbl_Status.text = "Available"
        } else {
            cell.lbl_Status.text = "Coming Soon"
        }
        return cell
    }
}

extension ClotheDetailViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayTags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCollectionViewCell", for: indexPath) as! HeaderCollectionViewCell
        cell.lbl_Title.text = arrayTags[indexPath.row]
        cell.lbl_Title.textColor = .white
        cell.view_Outer.backgroundColor = .black
        cell.view_Outer.layer.cornerRadius = 15.0
        cell.view_Outer.clipsToBounds = true
        return cell
    }
    
}

extension ClotheDetailViewController {
    
    func setuplocationMarker(coordinate: CLLocationCoordinate2D) {
        locationMarker = GMSMarker(position: coordinate)
        locationMarker.map = mapView
    }
    
    func setMarker(_ data:[String:Any]){
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: data["lat"] as! Double, longitude: data["long"] as! Double)
        marker.title = data["name"] as! String
        marker.map = mapView
        marker.icon = GMSMarker.markerImage(with: UIColor.red)

        mapView.selectedMarker = marker
        
        let camera = GMSCameraPosition.camera(withLatitude: data["lat"] as! Double, longitude: data["long"] as! Double, zoom: 16)
        mapView?.camera = camera
        mapView?.animate(to: camera)
        

    }
    
}
