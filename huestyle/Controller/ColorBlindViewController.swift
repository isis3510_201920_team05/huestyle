//
//  ColorBlindViewController.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 10/7/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class ColorBlindViewController: UIViewController {
    

    @IBOutlet weak var isColorBlindButton: UIButton!
    
    @IBOutlet weak var NotColorBlindButton: UIButton!
    
    @IBOutlet weak var labelErrorMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpElements()
        // Do any additional setup after loading the view.
    }
    
    func setUpElements() {
        // Hide the error label
        labelErrorMessage.alpha = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func isColorBlindTapped(_ sender: Any) {
        
        
        let id = String(Auth.auth().currentUser!.uid);
                
        let db = Firestore.firestore()
                
        
        db.collection("users").document(id).updateData(["colorBlind":true]) { (error) in
            //print(db.collection("users").document(id))
            if error != nil {
                // Show error message
                self.labelErrorMessage.alpha = 1
                print("Error saving user data")
            }
        }
        
        db.collection("users")
        .whereField("uid", isEqualTo: id)
        .getDocuments() { (querySnapshot, err) in
            if let err = err {
                // Some error occured
                print("Error updating user data", err)
            } else {
                let document = querySnapshot?.documents.first
                document!.reference.updateData([
                    "colorBlind": true
                ])
                let colorBlindTypeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.ColorBlindTypeViewController) as? ColorBlindTypeViewController
                
                self.view.window?.rootViewController = colorBlindTypeViewController
                self.view.window?.makeKeyAndVisible()
            }
        }
        
        
        
        
        
    }
    
    @IBAction func notColorBlindTapped(_ sender: Any) {
        
        let selectTrendViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.SelectTrendViewController) as? SelectTrendViewController
        
        self.view.window?.rootViewController = selectTrendViewController
        self.view.window?.makeKeyAndVisible()
        
    }
    

}
