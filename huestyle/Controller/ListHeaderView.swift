//
//  ListHeaderView.swift
//  huestyle
//
//  Created by Oliver Amaya on 24/10/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit

class ListHeaderView: UICollectionReusableView {
    
    
    let imageView: UIImageView = {
        let iv = UIImageView(image: #imageLiteral(resourceName: "bannerFinal"))
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //Custom Code
        addSubview(imageView)
        imageView.fillSuperview()
      
    }
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
