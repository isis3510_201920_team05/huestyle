//
//  SignUpViewController.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 10/6/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class SignUpViewController: UIViewController, UITextFieldDelegate, ReachabilityObserverDelegate {

    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var ageTextField: UITextField!
    
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var errorMessageLabel: UILabel!
    
    var favoriteClothes: [String] = []
    
    @IBOutlet weak var loginButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        addReachabilityObserver()
        // Do any additional setup after loading the view.
        
        setUpElements()
        
        self.setupToHideKeyboardOnTapOnView()
        ageTextField.smartInsertDeleteType = UITextSmartInsertDeleteType.no
        ageTextField.delegate = self
        
    }
    
    func textField(_ ageTextField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = ageTextField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 3
    }
    
    func reachabilityChanged(_ isReachable: Bool) {
        if !isReachable {
            self.showError("Verify your internet conection")
        }
        if isReachable{
            self.dontShowError()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setUpElements() {
    
        // Hide the error label
        errorMessageLabel.alpha = 0

    }
    
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        let logInViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.LogInViewController) as? LogInViewController
        
        self.view.window?.rootViewController = logInViewController
        self.view.window?.makeKeyAndVisible()
    }
    
    func validateFields() -> String? {
        
        // Check that all fields are filled in
        if nameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            
            return "Please fill in all fields."
        }
        
        // Check if the password is secure
        let cleanedPassword = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if Utilities.isPasswordValid(cleanedPassword) == false {
            // Password isn't secure enough
            return "Please make sure your password is at least 8 characters, contains a special character and a number."
        }
        
        return nil
    }
    
    @IBAction func registerTapped(_ sender: Any) {
        // Validate the fields
        let error = validateFields()
        
        if error != nil {
            
            // There's something wrong with the fields, show error message
            showError(error!)
        }
        else {
            
            // Create cleaned versions of the data
            let name = nameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let age = ageTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            
            // Create the user
            Auth.auth().createUser(withEmail: email, password: password) { (result, err) in
                
                // Check for errors
                if err != nil {
                    
                    // There was an error creating the user
                    self.showError("Error creating user")
                }
                else {
                    
                    // User was created successfully, now store the first name and last name
                    let db = Firestore.firestore()
                    
                    db.collection("users").addDocument(data: ["name":name, "age":age, "colorBlind": false, "styleType": "All", "favorites": self.favoriteClothes, "uid": result!.user.uid ]) { (error) in
                        
                        if error != nil {
                            // Show error message
                            self.showError("Error saving user data")
                        }
                    }
                    
                    // Transition to the home screen
                    //self.performSegue(withIdentifier: "goToBlindQuestion", sender: nil)
                    let colorBlindViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.ColorBlindViewController) as? ColorBlindViewController
                    
                    self.view.window?.rootViewController = colorBlindViewController
                    self.view.window?.makeKeyAndVisible()
                }
                
            }
            
            
            
        }
    }
    
    func showError(_ message:String) {
        
        errorMessageLabel.text = message
        errorMessageLabel.alpha = 1
    }

    func dontShowError() {
        
        errorMessageLabel.text = ""
        errorMessageLabel.alpha = 0
    }
}
