//
//  ClotheCollectionViewCell.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 10/20/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class ClotheCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var ClotheImage: UIImageView!
    
    @IBOutlet weak var TitleLabel: UILabel!
    
    @IBOutlet weak var PriceLabel: UILabel!

    @IBOutlet weak var FavoriteButton: UIButton!
    
    var isFav = false
    
    var favoriteClothes: [String] = []

    let deselectedImage = UIImage(systemName: "heart") //UIImage(named: "heart")
    let selectedImage = UIImage(systemName: "heart.fill")//UIImage(named: "heart-filled")
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.ClotheImage.image = nil
/*        print(isFav)
        if isFav {
            FavoriteButton.backgroundColor = UIColor.black
        }
 */
    }
    
    
    
    @IBAction func favoritedTapped(_ sender: Any) {
        let uid = String(Auth.auth().currentUser!.uid);
        let db = Firestore.firestore()
        
        //FavoriteButton.setImage(UIImage(named: "heart.fill"), for: .normal)
        if(isFav == false){
//            FavoriteButton.backgroundColor = UIColor.black
FavoriteButton.setImage(selectedImage, for: .normal)
            isFav = true
            db.collection("users")
            .whereField("uid", isEqualTo: uid)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print("Error updating user data", err)
                } else {
                    
                    guard let document = querySnapshot?.documents.first else {
                      print("Error")
                      return
                    }
                    if self.favoriteClothes.contains(self.TitleLabel.text!) {
                        print("item ya es fav")
                    } else {
                        self.favoriteClothes = document.get("favorites") as! [String]
                        self.favoriteClothes.append(self.TitleLabel.text!)
                        document.reference.updateData([
                            "favorites": self.favoriteClothes
                        ])
                    }
                    
                }
            }
        } else {
            isFav = false
            FavoriteButton.setImage(deselectedImage, for: .normal)
            FavoriteButton.backgroundColor = UIColor.white
            db.collection("users")
            .whereField("uid", isEqualTo: uid)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    // Some error occured
                    print("Error updating user data", err)
                } else {
                    
                    guard let document = querySnapshot?.documents.first else {
                      print("Error")
                      return
                    }
                    self.favoriteClothes = document.get("favorites") as! [String]
                    //self.favoriteClothes
                    if let index = self.favoriteClothes.firstIndex(of: self.TitleLabel.text!) {
                        self.favoriteClothes.remove(at: index)
                    }
                    //self.favoriteClothes.append(self.TitleLabel.text!)
                    document.reference.updateData([
                        "favorites": self.favoriteClothes
                    ])
                }
            }
        }
    }
   
    
    
    
}
