//
//  ResetPasswordViewController.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 3/11/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit
import SwiftUI
import Firebase
import SVProgressHUD

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var emailLabel: UITextField!
    
    @IBOutlet weak var resetPasswordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupToHideKeyboardOnTapOnView()
        // Do any additional setup after loading the view.
    }
    

    
    @IBAction func resetPasswordTapped(_ sender: Any) {
        
        
        let email = emailLabel.text!.trimmingCharacters(in: .whitespacesAndNewlines)

        if emailLabel.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            let empty = UIAlertController(title: "Reset Failed", message: "Email cannot be empty", preferredStyle: .alert)
            empty.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(empty, animated: true, completion: nil)
        } else {
            Auth.auth().sendPasswordReset(withEmail: email, completion: { (error) in
                //Make sure you execute the following code on the main queue
                DispatchQueue.main.async {
                    //Use "if let" to access the error, if it is non-nil
                    if let error = error {
                        let resetFailedAlert = UIAlertController(title: "Reset Failed", message: error.localizedDescription, preferredStyle: .alert)
                        resetFailedAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(resetFailedAlert, animated: true, completion: nil)
                    } else {
                        let resetEmailSentAlert = UIAlertController(title: "Reset email sent successfully", message: "Check your email", preferredStyle: .alert)
                        resetEmailSentAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(resetEmailSentAlert, animated: true, completion: nil)
                    }
                }
            })
        }
    }
    
    @IBAction func backToLoginTapped(_ sender: Any) {
        let logInViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.LogInViewController) as? LogInViewController
        
        self.view.window?.rootViewController = logInViewController
        self.view.window?.makeKeyAndVisible()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

