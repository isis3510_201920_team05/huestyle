//
//  ColorBlindTypeViewController.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 10/8/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit

class ColorBlindTypeViewController: UIViewController {
    
    
    @IBOutlet weak var nextButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    

    @IBAction func nextButtonTapped(_ sender: Any) {
        let selectTrendViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.SelectTrendViewController) as? SelectTrendViewController
        
        self.view.window?.rootViewController = selectTrendViewController
        self.view.window?.makeKeyAndVisible()
    }
    
}
