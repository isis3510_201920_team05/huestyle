//
//  SelectTrendViewController.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 10/8/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class SelectTrendViewController: UIViewController {

    @IBOutlet weak var skipButton: UIButton!
    
    
    @IBOutlet weak var businessTrendbutton: UIButton!
    
    @IBOutlet weak var casualTrendbutton: UIButton!
    
    @IBOutlet weak var semicasualTrendbutton: UIButton!
    
    @IBOutlet weak var alternativeTrendbutton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func businessButtonTapped(_ sender: Any) {
        let id = String(Auth.auth().currentUser!.uid);
                
        let db = Firestore.firestore()
                
        
        db.collection("users").document(id).updateData(["styleType":"Business"]) { (error) in
            //print(db.collection("users").document(id))
            if error != nil {
                // Show error message
                print("Error saving user data")
            }
        }
        
        db.collection("users")
        .whereField("uid", isEqualTo: id)
        .getDocuments() { (querySnapshot, err) in
            if let err = err {
                // Some error occured
                print("Error updating user data", err)
            } else {
                let document = querySnapshot?.documents.first
                document!.reference.updateData([
                    "styleType": "Business"
                ])
                let homeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.HomeViewController) as? HomeViewController
                
                self.view.window?.rootViewController = homeViewController
                self.view.window?.makeKeyAndVisible()
            }
        }
    }
    
    @IBAction func casualButtonTapped(_ sender: Any) {
        let id = String(Auth.auth().currentUser!.uid);
                
        let db = Firestore.firestore()
                
        
        db.collection("users").document(id).updateData(["styleType":"Casual"]) { (error) in
            //print(db.collection("users").document(id))
            if error != nil {
                // Show error message
                print("Error saving user data")
            }
        }
        
        db.collection("users")
        .whereField("uid", isEqualTo: id)
        .getDocuments() { (querySnapshot, err) in
            if let err = err {
                // Some error occured
                print("Error updating user data", err)
            } else {
                let document = querySnapshot?.documents.first
                document!.reference.updateData([
                    "styleType": "Casual"
                ])
                let homeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.HomeViewController) as? HomeViewController
                
                self.view.window?.rootViewController = homeViewController
                self.view.window?.makeKeyAndVisible()
            }
        }
    }
    
    @IBAction func semicasualButtonTapped(_ sender: Any) {
        let id = String(Auth.auth().currentUser!.uid);
                
        let db = Firestore.firestore()
                
        
        db.collection("users").document(id).updateData(["styleType":"Semi-Casual"]) { (error) in
            //print(db.collection("users").document(id))
            if error != nil {
                // Show error message
                print("Error saving user data")
            }
        }
        
        db.collection("users")
        .whereField("uid", isEqualTo: id)
        .getDocuments() { (querySnapshot, err) in
            if let err = err {
                // Some error occured
                print("Error updating user data", err)
            } else {
                let document = querySnapshot?.documents.first
                document!.reference.updateData([
                    "styleType": "Semi-Casual"
                ])
                let homeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.HomeViewController) as? HomeViewController
                
                self.view.window?.rootViewController = homeViewController
                self.view.window?.makeKeyAndVisible()
            }
        }
    }
    
    @IBAction func alternativeButtonTapped(_ sender: Any) {
        let id = String(Auth.auth().currentUser!.uid);
                
        let db = Firestore.firestore()
                
        
        db.collection("users").document(id).updateData(["styleType":"Alternative"]) { (error) in
            //print(db.collection("users").document(id))
            if error != nil {
                // Show error message
                print("Error saving user data")
            }
        }
        
        db.collection("users")
        .whereField("uid", isEqualTo: id)
        .getDocuments() { (querySnapshot, err) in
            if let err = err {
                // Some error occured
                print("Error updating user data", err)
            } else {
                let document = querySnapshot?.documents.first
                document!.reference.updateData([
                    "styleType": "Alternative"
                ])
                let homeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.HomeViewController) as? HomeViewController
                
                self.view.window?.rootViewController = homeViewController
                self.view.window?.makeKeyAndVisible()
            }
        }
    }
    
    @IBAction func skipButtonTapped(_ sender: Any) {
        let homeViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.HomeViewController) as? HomeViewController
        
        self.view.window?.rootViewController = homeViewController
        self.view.window?.makeKeyAndVisible()
    }
    
}
