//
//  CategoriesVC.swift
//  huestyle
//
//  Created by Oliver Amaya on 10/11/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore
import SDWebImage


class CategoriesVC: UIViewController , UISearchBarDelegate{
    
    @IBOutlet weak var collectionView_Top: UICollectionView!
    @IBOutlet weak var collectionView_Products: UICollectionView!
    
    var db: Firestore!
    let uid = String(Auth.auth().currentUser!.uid);
    let deselectedImage = UIImage(systemName: "heart") //UIImage(named: "heart")
    let selectedImage = UIImage(systemName: "heart.fill")//UIImage(named: "heart-filled")
    var clothes = [Clothes]()
    var customImageFlowLayout: CustomImageFlowLayout!
    var favoriteClothes: [String] = []
    let array_Items = ["All Categories","T-Shirt","Pants","Jacket","Suit","Dress","Skirt","Sweater"]
    var selectedIndex = 0
    var totalProductCount = 20
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadFavList()
        loadImagesDB("", true,totalProductCount)
        customImageFlowLayout = CustomImageFlowLayout()
        collectionView_Products.collectionViewLayout = customImageFlowLayout
        collectionView_Top.register(UINib(nibName: "HeaderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HeaderCollectionViewCell")
    }
    
    
    func loadImagesDB (_ categories: String,_ first:Bool, _ id : Int) {
        if first {
            let clothingReference = Firestore.firestore().collection("clothing")
            var newDataClothes = [Clothes]()
            
            clothingReference.limit(to: id).order(by: "id").addSnapshotListener { (snapshot, _) in
                Firestore.firestore().collection("users")
                    .whereField("uid", isEqualTo: self.uid)
                    .getDocuments() { (querySnapshot, err) in
                        if let err = err {
                            // Some error occured
                            print("Error getting user data", err)
                        } else {
                            guard (querySnapshot?.documents.first) != nil else {
                                print("Error")
                                return
                            }
                            guard let snapshot = snapshot else {return}
                            for document in snapshot.documents{
                                let myData = document.data()
                                let url = myData["src"] as? String
                                let name = myData["name"] as? String
                                let price = myData["price"] as! Int
                                let sizes = myData["sizes"] as! String
                                let category = myData["category"] as? String
                                let tags = myData["tags"] as? String
                                let storess =  myData["stores"] as! [Int]
                                let clothesData = Clothes(url: url!, id: "", name: name!, price: price, sizes: sizes, tags: tags ?? "", category: category!, stores: storess)
                                newDataClothes.append(clothesData)
                            }
                            self.clothes = newDataClothes
                            self.collectionView_Products.reloadData()
                        }
                }
            }
            
        } else {
            let clothingReference = Firestore.firestore().collection("clothing").whereField("category", isEqualTo: categories)
            var newDataClothes = [Clothes]()
            clothingReference.limit(to: id).order(by: "id").addSnapshotListener { (snapshot, _) in
                Firestore.firestore().collection("users")
                    .whereField("uid", isEqualTo: self.uid)
                    .getDocuments() { (querySnapshot, err) in
                        if let err = err {
                            // Some error occured
                            print("Error getting user data", err)
                        } else {
                            guard (querySnapshot?.documents.first) != nil else {
                                print("Error")
                                return
                            }
                            guard let snapshot = snapshot else {return}
                            for document in snapshot.documents{
                                let myData = document.data()
                                let url = myData["src"] as? String
                                let name = myData["name"] as? String
                                let price = myData["price"] as! Int
                                let sizes = myData["sizes"] as! String
                                let category = myData["category"] as? String
                                let tags = myData["tags"] as? String
                                let storess =  myData["stores"] as! [Int]
                                let clothesData = Clothes(url: url!, id: "", name: name!, price: price, sizes: sizes, tags: tags ?? "", category: category!, stores: storess)
                                newDataClothes.append(clothesData)
                            }
                            self.clothes = newDataClothes
                            self.collectionView_Products.reloadData()
                        }
                }
            }
        }
    }
    
    func loadFavList(){
        let uid = String(Auth.auth().currentUser!.uid);
        let dbf = Firestore.firestore()
        dbf.collection("users")
            .whereField("uid", isEqualTo: uid)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting user data", err)
                } else {
                    guard let document = querySnapshot?.documents.first else {
                        print("Error")
                        return
                    }
                    self.favoriteClothes = document.get("favorites") as! [String]
                }
        }
    }
    
    @objc func TappedCell(sender: UITapGestureRecognizer ) {
        
    }
    
}

extension CategoriesVC: UICollectionViewDelegate, UICollectionViewDataSource  {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionView_Top {
            return array_Items.count
        }
        return clothes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionView_Top {
            let cell = collectionView_Top.dequeueReusableCell(withReuseIdentifier: "HeaderCollectionViewCell", for: indexPath) as! HeaderCollectionViewCell
            cell.lbl_Title.text = array_Items[indexPath.row]
            if selectedIndex == indexPath.row {
                cell.lbl_Title.font = UIFont.boldSystemFont(ofSize: 18.0)
            }
            return cell
        } else {
            let cell = collectionView_Products.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath) as! ClotheCollectionViewCell
            let clothe = clothes[indexPath.row]
            cell.ClotheImage.sd_setImage(with: URL(string: clothe.url!), placeholderImage: UIImage(named: "image1"))
            if favoriteClothes.contains(clothe.name!) {
                cell.FavoriteButton.setImage(selectedImage, for: .normal)
                cell.isFav = true
            } else {
                cell.FavoriteButton.setImage(deselectedImage, for: .normal)
                cell.isFav = false
            }
            cell.TitleLabel.text = clothe.name
            cell.PriceLabel.text = "Price: \(clothe.price ?? 0)"
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.borderWidth = 0.5
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == collectionView_Top {
        } else {
            if indexPath.item == self.clothes.count - 4 {
                self.totalProductCount += 20
                if indexPath.row == 0 {
                    self.loadImagesDB("", true,totalProductCount)
                }else {
                    self.loadImagesDB(array_Items[selectedIndex], true,totalProductCount)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionView_Top {
            self.totalProductCount = 20
            print(array_Items[indexPath.row])
            selectedIndex = indexPath.row
            if indexPath.row == 0 {
                loadImagesDB("", true,totalProductCount)
            } else {
                loadImagesDB(array_Items[indexPath.row], false,totalProductCount)
            }
            self.collectionView_Top.reloadData()
        } else {
            let clothe = clothes[indexPath.row]
            let clotheDetailViewController = self.storyboard?.instantiateViewController(identifier: Constants.Storyboard.ClotheDetailViewController) as! ClotheDetailViewController
            clotheDetailViewController.clothes = clothe
            clotheDetailViewController.controller = "CategoriesVC"
            self.navigationController?.pushViewController(clotheDetailViewController, animated: true)
        }
    }
    
}
