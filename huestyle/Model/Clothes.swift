//
//  Clothes.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 10/7/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore

struct Clothes{
    
    let id: String?
    let url: String?
    let name: String?
    let price: Int?
    let sizes: String?
    let tags: String?
    let category: String
    var stores: [Int]
    
    
    
    init(url: String, id: String, name:String, price: Int, sizes: String, tags: String, category: String, stores: [Int]){
        self.id = id
        self.url = url
        self.name = name
        self.price = price
        self.sizes = sizes
        self.tags = tags
        self.category = category
        self.stores = stores
    }
    
    
}
    
