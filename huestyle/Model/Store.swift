//
//  Store.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 23/10/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore

struct Store{
    
    let lat: String?
    let long: String?
    let name: String?
    
    
    
    init(id:Int, lat:String, long: String, name:String){
        self.lat = lat
        self.long = long
        self.name = name
    }
    
    
}
