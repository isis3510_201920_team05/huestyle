//
//  User.swift
//  huestyle
//
//  Created by Juan Pablo Gonzalez P on 10/7/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import Foundation
class UserModel {
    var email: String?
    var name: String?
    var id: String?
    var isColorBlind: Bool?
    var styleType: String?
    var favoriteClothes: [String] = []
}

extension UserModel {
    static func transformUser(dict: [String: Any], key: String) -> UserModel {
        let user = UserModel()
        user.email = dict["email"] as? String
        user.name = dict["name"] as? String
        user.isColorBlind = dict["colorBlind"] as? Bool
        user.styleType = dict["styleType"] as? String
        user.favoriteClothes = dict["favorites"] as! [String]
        user.id = key
        return user
    }
}
