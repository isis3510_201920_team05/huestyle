//
//  ListView.swift
//  huestyle
//
//  Created by Oliver Amaya on 22/10/19.
//  Copyright © 2019 Juan Pablo Gonzalez P. All rights reserved.
//

import SwiftUI
struct ListView : View {
    var body: some View {
        Text("This is a swiftUI view 👋")
    }
}


#if DEBUG
struct ListView_Previews: PreviewProvider {
  static var previews: some View {
    ListView()
  }
}
#endif
